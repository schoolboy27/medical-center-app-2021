package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Center;
import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.Specialty;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;

public class DoctorDtoV3 {

    private UserDto user;
    private Specialty specialty;

    @Digits(integer = 7, fraction = 2)
    @Min(10000)
    private double salary;

    private Center center;

    public DoctorDtoV3() {}

    public DoctorDtoV3(UserDto user, Specialty specialty, double salary, Center center) {
        this.user = user;
        this.specialty = specialty;
        this.salary = salary;
        this.center = center;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }

    public static DoctorDtoV3 doctorDtoFromDoctor (Doctor doctor) {
        return new DoctorDtoV3(
                UserDto.userDtoFromUser(doctor.getUser()),
                doctor.getSpecialty(),
                doctor.getSalary(),
                doctor.getCenter()
        );
    }


}
