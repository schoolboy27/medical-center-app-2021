package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Service;
import com.homecompany.medcenter.models.Treatment;

import java.time.LocalDateTime;

public class TreatmentDto {

    private int id;
    private Service service;
    private DoctorDtoV3 doctor;
    private UserDto patient;
    private LocalDateTime appointment;

    public TreatmentDto() {}

    public TreatmentDto(int id, Service serviceDto, DoctorDtoV3 doctorDto, UserDto userDto, LocalDateTime time) {
        this.id = id;
        this.service = serviceDto;
        this.doctor = doctorDto;
        this.patient = userDto;
        this.appointment = time;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public DoctorDtoV3 getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorDtoV3 doctor) {
        this.doctor = doctor;
    }

    public UserDto getPatient() {
        return patient;
    }

    public void setPatient(UserDto patient) {
        this.patient = patient;
    }

    public LocalDateTime getAppointment() {
        return appointment;
    }

    public void setAppointment(LocalDateTime appointment) {
        this.appointment = appointment;
    }

    public static TreatmentDto treatmentDtoFromTreatment(Treatment treatment) {
        return new TreatmentDto(
                treatment.getId(),
                treatment.getService(),
                DoctorDtoV3.doctorDtoFromDoctor(treatment.getDoctor()),
                UserDto.userDtoFromUser(treatment.getPatient()),
                treatment.getAppointment().toLocalDateTime()
        );
    }

    @Override
    public String toString() {
        return "TreatmentDto{" +
                "id=" + id +
                ", service=" + service +
                ", doctor=" + doctor +
                ", patient=" + patient +
                ", appointment=" + appointment +
                '}';
    }
}
