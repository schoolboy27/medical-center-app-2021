package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Message;

import javax.validation.constraints.NotBlank;
import java.time.LocalDateTime;

public class MessageDto {

    private int id;

    private UserDtoV2 sender;
    private UserDtoV2 recipient;
    private String topic;

    @NotBlank
    private String messageBody;

    private LocalDateTime messageTime;

    public MessageDto() {}

    public MessageDto(int id,
                      UserDtoV2 sender,
                      UserDtoV2 recipient,
                      String topic,
                      String messageBody,
                      LocalDateTime messageTime) {
        this.id = id;
        this.sender = sender;
        this.recipient = recipient;
        this.topic = topic;
        this.messageBody = messageBody;
        this.messageTime = messageTime;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UserDtoV2 getSender() {
        return sender;
    }

    public void setSender(UserDtoV2 sender) {
        this.sender = sender;
    }

    public UserDtoV2 getRecipient() {
        return recipient;
    }

    public void setRecipient(UserDtoV2 recipient) {
        this.recipient = recipient;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public LocalDateTime getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(LocalDateTime messageTime) {
        this.messageTime = messageTime;
    }

    public static MessageDto messageDtoFromMessage(Message message) {
        return new MessageDto(
                message.getId(),
                UserDtoV2.userDtoV2FromUser(message.getSender()),
                UserDtoV2.userDtoV2FromUser(message.getRecipient()),
                message.getTopic(),
                message.getMessageBody(),
                message.getMessageTime().toLocalDateTime()
        );
    }
}
