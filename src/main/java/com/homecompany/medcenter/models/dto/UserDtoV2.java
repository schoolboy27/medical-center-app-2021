package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.User;

public class UserDtoV2 {

    private int id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String role;

    public UserDtoV2(int id, String lastName, String firstName, String middleName, String role) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.role = role;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public static UserDtoV2 userDtoV2FromUser(User user) {
        return new UserDtoV2(
                user.getId(),
                user.getLastName(),
                user.getFirstName(),
                user.getMiddleName(),
                "User"
        );
    }

    public static UserDtoV2 userDtoV2FromAdmin(User user) {
        return new UserDtoV2(
                user.getId(),
                user.getLastName(),
                user.getFirstName(),
                user.getMiddleName(),
                "Admin"
        );
    }

    public static UserDtoV2 userDtoV2FromDoctor(Doctor doctor) {
        return new UserDtoV2(
                doctor.getUser().getId(),
                doctor.getUser().getLastName(),
                doctor.getUser().getFirstName(),
                doctor.getUser().getMiddleName(),
                doctor.getSpecialty().getName()
        );
    }


}
