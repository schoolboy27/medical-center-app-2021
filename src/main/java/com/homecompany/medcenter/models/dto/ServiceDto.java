package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Specialty;

import javax.validation.constraints.*;

public class ServiceDto {

    private int id;
    @NotBlank
    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 3, max = 30)
    private String name;

    private Specialty specialty;

    @NotNull
    @Digits(integer = 7, fraction = 2)
    @Min(100)
    private double price;

    public ServiceDto() {}

    public ServiceDto(String name, Specialty specialty, int price) {
        this.name = name;
        this.specialty = specialty;
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
