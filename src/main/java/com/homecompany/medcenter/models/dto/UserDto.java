package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Center;
import com.homecompany.medcenter.models.User;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Date;

public class UserDto {

    private int id;

    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 2, max = 20)
    private String lastName;

    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 2, max = 20)
    private String firstName;

    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 2, max = 20)
    private String middleName;

    @Email
    private String login;

    private Date birthDate;
    private String userRole;

    @Valid
    private Center center;

    public UserDto(int id, String lastName, String firstName, String middleName, String login, Date birthDate, String userRole, Center center) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.login = login;
        this.birthDate = birthDate;
        this.userRole = userRole;
        this.center = center;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", login='" + login + '\'' +
                ", birthDate=" + birthDate +
                ", userRole='" + userRole + '\'' +
                '}';
    }

    public static UserDto userDtoFromUser(User user) {
        return new UserDto(
                user.getId(),
                user.getLastName(),
                user.getFirstName(),
                user.getMiddleName(),
                user.getLogin(),
                user.getBirthDate(),
                user.getUserRole(),
                user.getCenter()
        );
    }

}
