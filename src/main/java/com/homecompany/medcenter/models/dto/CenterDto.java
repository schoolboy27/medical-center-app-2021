package com.homecompany.medcenter.models.dto;

public class CenterDto {

    private int locationId;

    public CenterDto() {}

    public CenterDto(int locationId) {
        this.locationId = locationId;
    }

    public int getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }
}
