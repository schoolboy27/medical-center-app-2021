package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Review;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class ReviewDto {

    private int id;
    private DoctorDtoV3 doctor;
    private UserDto patient;
    @NotBlank
    @Size(min = 5, max = 100)
    private String reviewBody;

    public ReviewDto() {}

    public ReviewDto(int id, DoctorDtoV3 doctor, UserDto patient, String reviewBody) {
        this.id = id;
        this.doctor = doctor;
        this.patient = patient;
        this.reviewBody = reviewBody;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public DoctorDtoV3 getDoctor() {
        return doctor;
    }

    public void setDoctor(DoctorDtoV3 doctor) {
        this.doctor = doctor;
    }

    public UserDto getPatient() {
        return patient;
    }

    public void setPatient(UserDto patient) {
        this.patient = patient;
    }

    public String getReviewBody() {
        return reviewBody;
    }

    public void setReviewBody(String reviewBody) {
        this.reviewBody = reviewBody;
    }

    public static ReviewDto reviewDtoFromReview(Review review) {
        return new ReviewDto(
                review.getId(),
                DoctorDtoV3.doctorDtoFromDoctor(review.getDoctor()),
                UserDto.userDtoFromUser(review.getPatient()),
                review.getReviewBody()
        );
    }
}
