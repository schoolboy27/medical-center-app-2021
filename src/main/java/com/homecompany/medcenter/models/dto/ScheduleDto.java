package com.homecompany.medcenter.models.dto;

import java.time.LocalDateTime;

public class ScheduleDto {

    private int doctor;
    private int patient;
    private LocalDateTime appointment;
    private LocalDateTime oldAppointment;

    public ScheduleDto() {}

    public ScheduleDto(int doctor, int patient, LocalDateTime appointment, LocalDateTime oldAppointment) {
        this.doctor = doctor;
        this.patient = patient;
        this.appointment = appointment;
        this.oldAppointment = oldAppointment;
    }

    public int getDoctor() {
        return doctor;
    }

    public void setDoctor(int doctor) {
        this.doctor = doctor;
    }

    public int getPatient() {
        return patient;
    }

    public void setPatient(int patient) {
        this.patient = patient;
    }

    public LocalDateTime getAppointment() {
        return appointment;
    }

    public void setAppointment(LocalDateTime appointment) {
        this.appointment = appointment;
    }

    public LocalDateTime getOldAppointment() {
        return oldAppointment;
    }

    public void setOldAppointment(LocalDateTime oldAppointment) {
        this.oldAppointment = oldAppointment;
    }


    @Override
    public String toString() {
        return "ScheduleDto{" +
                "doctor=" + doctor +
                ", patient=" + patient +
                ", appointment=" + appointment +
                ", oldAppointment=" + oldAppointment +
                '}';
    }
}
