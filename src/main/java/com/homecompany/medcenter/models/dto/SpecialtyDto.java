package com.homecompany.medcenter.models.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class SpecialtyDto {

    private int id;

    @NotBlank
    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 3, max = 30)
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "SpecialtyDto{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
