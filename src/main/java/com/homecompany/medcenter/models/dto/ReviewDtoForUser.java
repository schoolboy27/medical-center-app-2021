package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Review;

public class ReviewDtoForUser {

    private int id;
    private String doctorLastName;
    private String doctorFirstName;
    private String doctorMiddleName;
    private String specialty;

    private String userFirstName;

    private String reviewBody;

    public ReviewDtoForUser(int id, String doctorLastName, String doctorFirstName, String doctorMiddleName, String specialty, String userFirstName, String reviewBody) {
        this.id = id;
        this.doctorLastName = doctorLastName;
        this.doctorFirstName = doctorFirstName;
        this.doctorMiddleName = doctorMiddleName;
        this.specialty = specialty;
        this.userFirstName = userFirstName;
        this.reviewBody = reviewBody;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDoctorLastName() {
        return doctorLastName;
    }

    public void setDoctorLastName(String doctorLastName) {
        this.doctorLastName = doctorLastName;
    }

    public String getDoctorFirstName() {
        return doctorFirstName;
    }

    public void setDoctorFirstName(String doctorFirstName) {
        this.doctorFirstName = doctorFirstName;
    }

    public String getDoctorMiddleName() {
        return doctorMiddleName;
    }

    public void setDoctorMiddleName(String doctorMiddleName) {
        this.doctorMiddleName = doctorMiddleName;
    }

    public String getSpecialty() {
        return specialty;
    }

    public void setSpecialty(String specialty) {
        this.specialty = specialty;
    }

    public String getUserFirstName() {
        return userFirstName;
    }

    public void setUserFirstName(String userFirstName) {
        this.userFirstName = userFirstName;
    }

    public String getReviewBody() {
        return reviewBody;
    }

    public void setReviewBody(String reviewBody) {
        this.reviewBody = reviewBody;
    }

    public static ReviewDtoForUser reviewDtoForUserFromReview(Review review) {
        return new ReviewDtoForUser(
                review.getId(),
                review.getDoctor().getUser().getLastName(),
                review.getDoctor().getUser().getFirstName(),
                review.getDoctor().getUser().getMiddleName(),
                review.getDoctor().getSpecialty().getName(),
                review.getPatient().getFirstName(),
                review.getReviewBody()
        );
    }
}
