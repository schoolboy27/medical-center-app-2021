package com.homecompany.medcenter.models.dto;

import javax.validation.constraints.*;

public class LocationDto {

    private int id;

    @NotBlank
    @Size(min = 3, max = 30)
    private String street;

    @NotBlank
    @Pattern(regexp = "^[1-9][0-9]{0,2}[a-z]?$")
    private String building;

    @NotNull
    @Min(1)
    private int apartment;

    public LocationDto() {}

    public LocationDto(String street, String building, int apartment) {
        this.street = street;
        this.building = building;
        this.apartment = apartment;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public int getApartment() {
        return apartment;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }
}
