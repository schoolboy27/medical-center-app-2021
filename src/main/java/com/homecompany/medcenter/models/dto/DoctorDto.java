package com.homecompany.medcenter.models.dto;

import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.Specialty;

public class DoctorDto {

    private int id;
    private String lastName;
    private String firstName;
    private String middleName;
    private Specialty specialty;

    public DoctorDto(int id, String lastName, String firstName, String middleName, Specialty specialty) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.specialty = specialty;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    @Override
    public String toString() {
        return "DoctorDto{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", specialty='" + specialty + '\'' +
                '}';
    }

    public static DoctorDto doctorDtoFromDoctor(Doctor doctor) {
        return new DoctorDto(
                doctor.getId(),
                doctor.getUser().getLastName(),
                doctor.getUser().getFirstName(),
                doctor.getUser().getMiddleName(),
                doctor.getSpecialty()
        );
    }
}
