package com.homecompany.medcenter.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "messages")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "sender_id", referencedColumnName = "id")
    @Valid
    private User sender;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "recipient_id", referencedColumnName = "id")
    @Valid
    private User recipient;

    @Column(name = "topic")
    private String topic;

    @Column(name = "message_body")
    @NotBlank
    private String messageBody;

    @Column(name = "message_time", nullable = true)
    private Timestamp messageTime;


    public Message() {
    }

    public Message(User sender,
                   User recipient,
                   String topic,
                   String messageBody) {//,
                   //Timestamp when) {
        this.sender = sender;
        this.recipient = recipient;
        this.topic = topic;
        this.messageBody = messageBody;
        //this.when = when;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getRecipient() {
        return recipient;
    }

    public void setRecipient(User recipient) {
        this.recipient = recipient;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public Timestamp getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(LocalDateTime messageTime) {
        this.messageTime = Timestamp.valueOf(messageTime);
    }

    /*public Timestamp getWhen() {
        return when;
    }

    public void setWhen(Timestamp when) {
        this.when = when;
    }*/

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", sender=" + sender +
                ", recipient=" + recipient +
                ", topic='" + topic + '\'' +
                ", messageBody='" + messageBody + '\'' +
                ", when=" + messageTime +
                '}';
    }
}
