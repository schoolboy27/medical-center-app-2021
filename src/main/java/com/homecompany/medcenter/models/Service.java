package com.homecompany.medcenter.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.*;
import java.io.Serializable;

@Entity
@Table(name = "services")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Service implements Serializable {
    private static final long serialVersionUID = -5527566248003233342L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 3, max = 30)
    @Column(name = "name")
    private String name;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "specialty_id")
    @Valid
    private Specialty specialty;

    @NotNull
    @Digits(integer = 7, fraction = 2)
    @Min(100)
    @Column(name = "price")
    private double price;

    public Service() {}

    public Service(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public Service(String name, Specialty specialty, double price) {
        this.name = name;
        this.specialty = specialty;
        this.price = price;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Service{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", specialty=" + specialty +
                ", price=" + price +
                '}';
    }
}
