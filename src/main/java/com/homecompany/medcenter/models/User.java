package com.homecompany.medcenter.models;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name="users")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class User implements Serializable {
    private static final long serialVersionUID = -5527566248002293342L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "lastname", nullable = false)
    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 2, max = 20)
    private String lastName;

    @Column(name = "firstname", nullable = false)
    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 2, max = 20)
    private String firstName;

    @Column(name = "middlename", nullable = false)
    @Pattern(regexp = "^[A-Z][a-z]*$")
    @Size(min = 2, max = 20)
    private String middleName;

    @Column(name = "login", nullable = false)
    @Email
    private String login;

    @Column(name = "password", nullable = false)
    @Size(min = 8)
    private String password;

    @Column(name = "birthdate")
    private Date birthDate;

    @Column(name = "role")
    private String userRole;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "center_id")
    @Valid
    //@JsonIgnore
    private Center center;

    public User() {}

    public User(String lastName,
                String firstName,
                String middleName,
                String login,
                String password,
                Date birthDate,
                String role,
                Center center) {

        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.login = login;
        this.password = password;
        this.birthDate = birthDate;
        this.userRole = role;
        this.center = center;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String role) {
        this.userRole = role;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", birthDate=" + birthDate +
                ", role='" + userRole + '\'' +
                ", center=" + center +
                '}';
    }
}
