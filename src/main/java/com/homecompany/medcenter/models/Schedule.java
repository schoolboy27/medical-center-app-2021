package com.homecompany.medcenter.models;

import javax.persistence.*;
import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "schedules")
public class Schedule {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    @Valid
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    private User patient;

    @Column(name = "date")
    private Timestamp date;

    @Column(name = "comments")
    private String comments;

    public Schedule() {}

    public Schedule(int id, Doctor doctor, User patient, Timestamp date, String comments) {
        this.id = id;
        this.doctor = doctor;
        this.patient = patient;
        this.date = date;
        this.comments = comments;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = Timestamp.valueOf(date);
    }


    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "Schedule{" +
                "id=" + id +
                ", doctor=" + doctor +
                ", patient=" + patient +
                ", date=" + date +
                ", comments='" + comments + '\'' +
                '}';
    }
}
