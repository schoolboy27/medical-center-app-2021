package com.homecompany.medcenter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
@Table(name = "locations")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class Location {//implements Serializable {

    //private static final long serialVersionUID = -5527566248002296042L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Size(min = 3, max = 30)
    @Column(name = "street")
    private String street;

    @NotBlank
    @Pattern(regexp = "^[1-9][0-9]{0,2}[a-z]?$")
    @Column(name = "building")
    private String building;

    @NotNull
    @Min(1)
    @Column(name = "apartment")
    private int apartment;

    public Location() {
    }

    public Location(String street, String building, int apartment) {
        this.street = street;
        this.building = building;
        this.apartment = apartment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getBuilding() {
        return building;
    }

    public void setBuilding(String building) {
        this.building = building;
    }

    public int getApartment() {
        return apartment;
    }

    public void setApartment(int apartment) {
        this.apartment = apartment;
    }

    @Override
    public String toString() {
        return "Location{" +
                "id=" + id +
                ", street='" + street + '\'' +
                ", building='" + building + '\'' +
                ", apartment=" + apartment +
                '}';
    }
}
