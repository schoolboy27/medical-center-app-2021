package com.homecompany.medcenter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.Min;
import java.io.Serializable;

@Entity
@Table(name = "doctors")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Doctor implements Serializable {

    @Id
    private int id;

    @OneToOne(fetch = FetchType.EAGER)
    @MapsId
    @JoinColumn(name = "id")
    @Valid
    private User user;

    @ManyToOne
    @JoinColumn(name = "specialty_id")
    @Valid
    private Specialty specialty;

    @Column(name = "salary")
    @Digits(integer = 7, fraction = 2)
    @Min(10000)
    private double salary;

    @ManyToOne
    @JoinColumn(name = "center_id")
    @Valid
    private Center center;

    public Doctor() {
    }

    public Doctor(Specialty specialty, double salary, Center center) {
        this.specialty = specialty;
        this.salary = salary;
        this.center = center;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Specialty getSpecialty() {
        return specialty;
    }

    public void setSpecialty(Specialty specialty) {
        this.specialty = specialty;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Center getCenter() {
        return center;
    }

    public void setCenter(Center center) {
        this.center = center;
    }

    @Override
    public String toString() {
        return "Doctor{" +
                "id=" + id +
                ", specialty=" + specialty +
                ", salary=" + salary +
                ", center=" + center +
                '}';
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
