package com.homecompany.medcenter.models;

import javax.validation.constraints.Email;
import javax.validation.constraints.Min;

public class AuthBody {

    @Email
    private String username;
    
    @Min(8)
    private String password;

    public AuthBody() {
    }

    public AuthBody(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "AuthBody{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
