package com.homecompany.medcenter.models;

import javax.persistence.*;
import javax.validation.Valid;
import java.io.Serializable;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
@Table(name = "treatments")
//@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Treatment implements Serializable {
    private static final long serialVersionUID = -55275648002296042L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "service_id")
    @Valid
    private Service service;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    @Valid
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    @Valid
    private User patient;

    @Column(name = "appointment", nullable = true)
    private Timestamp appointment;

    public Treatment() {}

    public Treatment(Service service,
                     Doctor doctor,
                     User patient,
                     LocalDateTime appointment) {
        this.service = service;
        this.doctor = doctor;
        this.patient = patient;
        this.appointment = Timestamp.valueOf(appointment);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Service getService() {
        return service;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

     public Timestamp getAppointment() {
        return appointment;
    }

    public void setAppointment(LocalDateTime appointment) {
        this.appointment = Timestamp.valueOf(appointment);
    }

    @Override
    public String toString() {
        return "Treatment{" +
                "id=" + id +
                ", service=" + service +
                ", doctor=" + doctor +
                ", patient=" + patient +
                ", appointment=" + appointment +
                '}';
    }
}
