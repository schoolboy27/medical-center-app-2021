package com.homecompany.medcenter.models;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "reviews")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Review implements Serializable {

    private static final long serialVersionUID = -55275662480022960L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doctor_id")
    @Valid
    private Doctor doctor;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "patient_id")
    @Valid
    private User patient;

    @NotBlank
    @Size(min = 5, max = 100)
    @Column(name = "review_body")
    private String reviewBody;

    public Review() {
    }

    public Review(Doctor doctor, User patient, String reviewBody) {
        this.doctor = doctor;
        this.patient = patient;
        this.reviewBody = reviewBody;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Doctor getDoctor() {
        return doctor;
    }

    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }

    public User getPatient() {
        return patient;
    }

    public void setPatient(User patient) {
        this.patient = patient;
    }

    public String getReviewBody() {
        return reviewBody;
    }

    public void setReviewBody(String reviewBody) {
        this.reviewBody = reviewBody;
    }

    @Override
    public String toString() {
        return "Review{" +
                "id=" + id +
                ", doctor=" + doctor +
                ", user=" + patient +
                ", reviewBody='" + reviewBody + '\'' +
                '}';
    }
}
