package com.homecompany.medcenter.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "specialties")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Specialty implements Serializable {

    private static final long serialVersionUID = -5527566248001196042L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Pattern(regexp = "^[A-Z][a-z-]*$")
    @Size(min = 3, max = 30)
    @Column(name = "name")
    private String name;

    public Specialty() {}

    public Specialty(String name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Specialty{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
