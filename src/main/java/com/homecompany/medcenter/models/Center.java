package com.homecompany.medcenter.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "centers")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Center {//implements Serializable {
    //private static final long serialVersionUID = -5527566248002296042L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @OneToOne(fetch = FetchType.EAGER)//, cascade = CascadeType.REFRESH)
    @JoinColumn(name = "location_id")
    @Valid
    private Location location;

    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "center_id", referencedColumnName = "id")
    //@JsonBackReference -> check that and @JsonIdentityInfo ->@JsonIdentityInfo(
    //  generator = ObjectIdGenerators.PropertyGenerator.class,
    //  property = "id")
    @JsonIgnore
    private List<User> users = new ArrayList<>();


    public Center() {
    }

    public Center(Location location, List<User> users) {
        this.location = location;
        this.users = users;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public void addUser(User user) {
        users.add(user);
    }

    @Override
    public String toString() {
        return "Center{" +
                "id=" + id +
                ", location=" + location +
                //", users=" + users +
                '}';
    }
}
