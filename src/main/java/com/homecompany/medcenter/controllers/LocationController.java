package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.Location;
import com.homecompany.medcenter.models.dto.LocationDto;
import com.homecompany.medcenter.services.LocationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/locations")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @GetMapping("/all")
    public List<Location> retrieveAllLocations() {
        return locationService.retrieveAllLocations();
    }

    @GetMapping("/{locationId}")
    public Location retrieveLocationById(@PathVariable Integer locationId) {
        return locationService.retrieveLocationById(locationId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public Location createLoc(@Valid @RequestBody Location location) {
        return locationService.createLocation(location);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{locationId}")
    public ResponseEntity<String> deleteLocationById(@PathVariable Integer locationId) {
        return locationService.deleteLocationById(locationId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update/{locationId}")
    public Location updateLocationById(@PathVariable Integer locationId,
                                       @Valid @RequestBody LocationDto locationDto) {
        return locationService.updateLocationById(locationId, locationDto);
    }
}
