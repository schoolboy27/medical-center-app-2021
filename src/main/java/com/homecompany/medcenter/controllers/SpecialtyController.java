package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.Specialty;
import com.homecompany.medcenter.models.dto.SpecialtyDto;
import com.homecompany.medcenter.services.SpecialtyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/specialties")
public class SpecialtyController {

    @Autowired
    private SpecialtyService specialtyService;

    @GetMapping("/all")
    public List<Specialty> retrieveAllSpecialties() {
        return specialtyService.retrieveAllSpecialties();
    }

    @GetMapping("/{specialtyId}")
    public Specialty retrieveSpecialtyById(@PathVariable Integer specialtyId) {
        return specialtyService.retrieveSpecialtyById(specialtyId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create")
    public Specialty createSpecialty(@Valid @RequestBody Specialty specialty) {
        return specialtyService.createSpecialty(specialty);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{specialtyId}")
    public ResponseEntity<Object> deleteSpecialtyById(@PathVariable Integer specialtyId) {
        return specialtyService.deleteSpecialtyById(specialtyId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update")
    public Specialty updateSpecialty(@Valid @RequestBody SpecialtyDto specialtyDto) {
        return specialtyService.updateSpecialty(specialtyDto);
    }
}
