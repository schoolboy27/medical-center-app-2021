package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.AuthBody;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.UserDto;
import com.homecompany.medcenter.models.dto.UserDtoV2;
import com.homecompany.medcenter.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/users")

public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/all")
    @PreAuthorize("hasAnyRole('ADMIN','DOCTOR')")
    public List<UserDto> retrieveAllUsers() {
        return userService.retrieveAllUsers();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all/interlocutors")
    public List<UserDtoV2> retrieveAllInterlocutorsForAdmin() {
        return userService.retrieveAllInterlocutorsForAdmin();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAnyRole('ADMIN', 'DOCTOR')")
    public UserDto retrieveUserById(@PathVariable(value = "id") Integer userId) {
        return userService.retrieveUserById(userId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all/not/doctors")
    public List<UserDtoV2> retrieveUsersNotDoctors() {
        return userService.retrieveUsersNotDoctors();
    }

    @PostMapping("/find/username")
    public UserDto retrieveUserByUsername(@RequestBody String username) {
        return userService.retrieveUserByUsername(username);
    }

    @PostMapping("/create")
    public ResponseEntity<Object> createUser(@Valid @RequestBody User user) {
        return userService.createUser(user);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Object> deleteUserById(@PathVariable(value = "id") Integer userId) {
        return userService.deleteUserById(userId);
    }

    @PostMapping("/update/{userId}")
    public UserDto updateUserById(
            @PathVariable Integer userId,
            @RequestBody UserDto userDto) {
        return userService.updateUserById(userId, userDto);
    }

    @PostMapping("/login")
    public ResponseEntity<Object> authenticateUser(@RequestBody AuthBody authBody, HttpServletRequest request) {
        return userService.doLogin(authBody, request);
    }

    @PostMapping("/logout")
    public void logout(HttpServletRequest request, HttpServletResponse response) {
        SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
        securityContextLogoutHandler.logout(request, response, null);
    }

}
