package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.Center;
import com.homecompany.medcenter.models.dto.CenterDto;
import com.homecompany.medcenter.services.CenterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/centers")
public class CenterController {

    @Autowired
    private CenterService centerService;

    //???
    @GetMapping("/all")
    public ResponseEntity<List<Center>> retrieveAllCenters() {
        return centerService.retrieveAllCenters();
    }

    @GetMapping("/{centerId}")
    public Center retrieveCenterById(@PathVariable Integer centerId){
        return centerService.retrieveCenterById(centerId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create/loc/{locId}")
    public Center createCenterWithLoc(@PathVariable Integer locId,
                               @Valid @RequestBody Center center) {
        return centerService.createCenterWithLoc(locId, center);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{centerId}")
    public ResponseEntity<String> deleteCenterById(@PathVariable Integer centerId) {
        return centerService.deleteCenterById(centerId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update/{centerId}")
    public Center updateCenterById(@PathVariable Integer centerId,
                               @Valid @RequestBody CenterDto centerDto) {
        return centerService.updateCenterById(centerId, centerDto);
    }
}
