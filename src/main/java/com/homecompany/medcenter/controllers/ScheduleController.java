package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.dto.ScheduleDto;
import com.homecompany.medcenter.services.ScheduleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalTime;
import java.util.List;

@RestController
@RequestMapping("/schedule")
public class ScheduleController {

    @Autowired
    private ScheduleService scheduleService;

    @PostMapping("/doc/{doctorId}")
    public List<LocalTime> getDoctorTime(@PathVariable Integer doctorId,
                              @RequestBody String date) {
        return scheduleService.getDoctorTime(doctorId, date);
    }

    @PostMapping("update")
    public ResponseEntity<String> updateDoctorSchedule(@RequestBody ScheduleDto scheduleDto) {
        return scheduleService.updateDoctorSchedule(scheduleDto);
    }

}
