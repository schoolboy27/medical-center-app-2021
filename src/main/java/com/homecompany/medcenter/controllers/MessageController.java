package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.dto.MessageDto;
import com.homecompany.medcenter.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/messages")
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public List<MessageDto> retrieveAllMessages() {
        return messageService.retrieveAllMessages();
    }

    @GetMapping("/id/{messageId}")
    public MessageDto retrieveMessageById(@PathVariable Integer messageId) {
        return messageService.retrieveMessageById(messageId);
    }

    @GetMapping("/find/by/users/{userOneId}/{userTwoId}")
    public List<MessageDto> retrieveMessagesByTwoUsers(@PathVariable Integer userOneId,
                                                       @PathVariable Integer userTwoId) {
        return messageService.retrieveMessagesByTwoUsers(userOneId, userTwoId);
    }

    @PostMapping("/create/from/{fromId}/to/{toId}")
    public MessageDto createMessage(@PathVariable Integer fromId,
                                 @PathVariable Integer toId,
                                 @Valid @RequestBody MessageDto message) {
        return messageService.createMessage(fromId, toId, message);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{messageId}")
    public ResponseEntity<String> deleteMessageById(@PathVariable Integer messageId) {
        return messageService.deleteMessageById(messageId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update/{messageId}")
    public MessageDto updateMessageById(@PathVariable Integer messageId,
                                     @Valid @RequestBody MessageDto messageDto) {
        return messageService.updateMessageById(messageId, messageDto);
    }
}
