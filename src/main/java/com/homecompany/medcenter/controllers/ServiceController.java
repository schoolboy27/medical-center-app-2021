package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.Service;
import com.homecompany.medcenter.models.dto.ServiceDto;
import com.homecompany.medcenter.services.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/centerservices")
public class ServiceController {

    @Autowired
    private ServiceService serviceService;

    @GetMapping("/all")
    public List<Service> retrieveAllServices() {
        return serviceService.retrieveAllServices();
    }

    @GetMapping("/{serviceId}")
    public Service retrieveServiceById(@PathVariable Integer serviceId) {
        return serviceService.retrieveServiceById(serviceId);
    }

    @GetMapping("/spec/{specialtyId}")
    public List<Service> retrieveServicesBySpecialtyId(@PathVariable Integer specialtyId) {
        return serviceService.retrieveServicesBySpecialtyId(specialtyId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create/specialty/{specialtyId}")
    public Service createServiceWithSpecialty(@PathVariable Integer specialtyId,
                                         @Valid @RequestBody Service service) {
        return serviceService.createServiceWithSpecialty(specialtyId, service);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{serviceId}")
    public ResponseEntity<String> deleteServiceById(@PathVariable Integer serviceId) {
        return serviceService.deleteServiceById(serviceId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update/{serviceId}/spec/{specialtyId}")
    public Service updateServiceById(
            @PathVariable Integer serviceId,
            @PathVariable Integer specialtyId,
            @Valid @RequestBody ServiceDto serviceDto) {
        return serviceService.updateServiceById(serviceId, specialtyId, serviceDto);
    }
}
