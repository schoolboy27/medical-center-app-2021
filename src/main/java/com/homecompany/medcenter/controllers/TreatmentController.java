package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.dto.TreatmentDto;
import com.homecompany.medcenter.models.dto.UserDtoV2;
import com.homecompany.medcenter.services.TreatmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/treatments")
public class TreatmentController {

    @Autowired
    private TreatmentService treatmentService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public List<TreatmentDto> retrieveAllTreatments() {
        return treatmentService.retrieveAllTreatments();
    }


    @GetMapping("/{treatmentId}")
    public TreatmentDto retrieveTreatmentById(@PathVariable Integer treatmentId) {
        return treatmentService.retrieveTreatmentById(treatmentId);
    }

    @GetMapping("/find/doctor/{doctorId}")
    public List<TreatmentDto> retrieveTreatmentsByDoctorId(@PathVariable Integer doctorId) {
        return  treatmentService.retrieveTreatmentsByDoctorId(doctorId);
    }

    @GetMapping("/find/patient/{patientId}")
    public List<TreatmentDto> retrieveTreatmentsByPatientId(@PathVariable Integer patientId) {
        System.out.println(patientId);
        return  treatmentService.retrieveTreatmentsByPatientId(patientId);
    }

    @GetMapping("find/doctors/by/patient/{patientId}")
    public List<UserDtoV2> retrieveDoctorsByPatientId(@PathVariable Integer patientId) {
        return treatmentService.retrieveDoctorsByPatientId(patientId);
    }

    @PreAuthorize("hasAnyRole('DOCTOR','ADMIN')")
    @GetMapping("find/interlocutors/by/doctor/{doctorId}")
    public List<UserDtoV2> retrieveInterlocutorsForDoctor(@PathVariable Integer doctorId) {
        return treatmentService.retrieveInterlocutorsForDoctor(doctorId);
    }


    @PostMapping("/create/service/{serviceId}/doc/{doctorId}/patient/{patientId}")
    public TreatmentDto createTreatment(@PathVariable Integer serviceId,
                                     @PathVariable Integer doctorId,
                                     @PathVariable Integer patientId,
                                     @RequestBody TreatmentDto treatmentDto) {
        return treatmentService.createTreatment(serviceId, doctorId, patientId, treatmentDto.getAppointment());
    }

    @DeleteMapping("/delete/{treatmentId}")
    public ResponseEntity<Object> deleteTreatmentById(@PathVariable Integer treatmentId) {
        return treatmentService.deleteTreatmentById(treatmentId);
    }

    @PostMapping("/update/{treatmentId}/serv/{serviceId}/doc/{doctorId}/pat/{patientId}")
    public TreatmentDto updateTreatmentById(@PathVariable Integer treatmentId,
                                           @PathVariable Integer serviceId,
                                           @PathVariable Integer doctorId,
                                           @PathVariable Integer patientId,
                                           @RequestBody TreatmentDto body) {
        return treatmentService.updateTreatmentById(treatmentId, serviceId, doctorId, patientId, body);
    }

}
