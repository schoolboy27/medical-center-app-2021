package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.Review;
import com.homecompany.medcenter.models.dto.ReviewDto;
import com.homecompany.medcenter.models.dto.ReviewDtoForUser;
import com.homecompany.medcenter.services.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/reviews")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/all")
    public List<ReviewDto> retrieveAllReviews() {
        return reviewService.retrieveAllReviews();
    }

    @GetMapping("/all/to/user")
    public List<ReviewDtoForUser> retrieveAllReviewsToUser() {
        return reviewService.retrieveAllReviewsToUser();
    }

    @GetMapping("/{reviewId}")
    public ReviewDto retrieveReviewById(@PathVariable Integer reviewId) {
        return reviewService.retrieveReviewById(reviewId);
    }

    @PostMapping("/create/doctor/{doctorId}/patient/{patientId}")
    public ReviewDto createReview(@PathVariable Integer doctorId,
                               @PathVariable Integer patientId,
                               @Valid @RequestBody Review review) {
        return reviewService.createReview(doctorId, patientId, review);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{reviewId}")
    public ResponseEntity<String> deleteReviewById(@PathVariable Integer reviewId) {
        return reviewService.deleteReviewById(reviewId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update/{reviewId}/doc/{doctorId}/pat/{patientId}")
    public ReviewDto updateReviewById(@PathVariable Integer reviewId,
                                   @PathVariable Integer doctorId,
                                   @PathVariable Integer patientId,
                                   @RequestBody ReviewDto reviewDto) {
        return reviewService.updateReviewById(reviewId, doctorId, patientId, reviewDto);
    }
}
