package com.homecompany.medcenter.controllers;

import com.homecompany.medcenter.models.dto.DoctorDto;
import com.homecompany.medcenter.models.dto.DoctorDtoV3;
import com.homecompany.medcenter.services.DoctorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/doctors")
public class DoctorController {

    @Autowired
    private DoctorService doctorService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping("/all")
    public List<DoctorDtoV3> retrieveAllDoctors() {
        return doctorService.retrieveAllDoctors();
    }

    @GetMapping("/all/to/user")
    public List<DoctorDto> retrieveAllDoctorsToUser() {
        return doctorService.retrieveAllDoctorsToUser();
    }

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{doctorId}")
    public DoctorDtoV3 retrieveDoctorById(@PathVariable Integer doctorId) {
        return doctorService.retrieveDoctorById(doctorId);
    }

    @GetMapping("/spec/{specialtyId}")
    public List<DoctorDto> retrieveDoctorsBySpecialtyId(@PathVariable Integer specialtyId) {
        return doctorService.retrieveDoctorsBySpecialtyId(specialtyId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/create/user/{userId}/spec/{specialtyId}/cntr/{centerId}")
    public DoctorDtoV3 createDoctor(@PathVariable Integer userId,
                               @PathVariable Integer specialtyId,
                               @PathVariable Integer centerId,
                               @Valid @RequestBody DoctorDtoV3 doctor) {
        return doctorService.createDoctor(userId, specialtyId, centerId, doctor);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/delete/{doctorId}")
    public ResponseEntity<String> deleteDoctorById(@PathVariable Integer doctorId) {
        return doctorService.deleteDoctorById(doctorId);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("/update/doc/{doctorId}/cntr/{centerId}/spec/{specialtyId}")
    public DoctorDtoV3 updateDoctorById(@PathVariable Integer doctorId,
                                   @PathVariable Integer centerId,
                                   @PathVariable Integer specialtyId,
                                   @Valid @RequestBody DoctorDtoV3 doctorDto) {
        return doctorService.updateDoctorById(doctorId, centerId, specialtyId, doctorDto);
    }
}
