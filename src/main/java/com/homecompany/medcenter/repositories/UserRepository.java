package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

    Optional<User> findByLogin(String login);

    @Query(value = "select u.id, u.lastName, u.firstName, u.middleName, u.login, u.password, u.center_id, u.birthdate, u.role from users as u where u.id not in (select d.id from doctors as d where u.id = d.id);", nativeQuery = true)
    List<User> findAllUsersNotDoctors();

    @Query(value = "select * from users as u where u.role = 'ROLE_ADMIN';", nativeQuery = true)
    List<User> findAdmins();
}
