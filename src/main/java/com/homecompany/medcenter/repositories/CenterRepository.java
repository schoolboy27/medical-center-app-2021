package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Center;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CenterRepository extends JpaRepository<Center, Integer> {

}
