package com.homecompany.medcenter.repositories;


import com.homecompany.medcenter.models.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;
import java.util.List;

public interface ScheduleRepository extends JpaRepository<Schedule, Integer> {

    List<Schedule> findByDoctorId(Integer doctorId);

    Schedule findByDoctorIdAndDate(Integer doctorId, Timestamp date);
}
