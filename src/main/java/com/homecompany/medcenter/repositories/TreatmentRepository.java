package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Treatment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TreatmentRepository extends JpaRepository<Treatment, Integer> {

    List<Treatment> findTreatmentsByPatientIdOrderByAppointmentDesc(Integer patientId);

    List<Treatment> findTreatmentsByDoctorIdOrderByAppointmentDesc(Integer doctorId);

}
