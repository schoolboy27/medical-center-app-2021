package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Service;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ServiceRepository extends JpaRepository<Service, Integer> {
    List<Service> findBySpecialtyId(Integer specialtyId);

    


}
