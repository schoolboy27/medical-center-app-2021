package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Integer> {
}
