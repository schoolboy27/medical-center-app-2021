package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Message;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Integer> {

    @Query(value = "select * from messages  where ((sender_id = :first and recipient_id = :second) or (sender_id = :second and recipient_id = :first)) order by message_time", nativeQuery = true)
    List<Message> findByTwoUsers(@Param("first") Integer first, @Param("second") Integer second);
}
