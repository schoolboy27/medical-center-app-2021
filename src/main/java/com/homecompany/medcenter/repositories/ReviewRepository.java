package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Review;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReviewRepository extends JpaRepository<Review, Integer> {
}
