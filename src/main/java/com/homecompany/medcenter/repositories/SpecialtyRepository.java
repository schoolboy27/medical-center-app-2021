package com.homecompany.medcenter.repositories;

import com.homecompany.medcenter.models.Specialty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpecialtyRepository extends JpaRepository<Specialty, Integer> {

}
