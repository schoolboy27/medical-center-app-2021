package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Message;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.MessageDto;
import com.homecompany.medcenter.repositories.MessageRepository;
import com.homecompany.medcenter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    private static final String NOT_FOUND_MESSAGE_BY_ID = "Not found message by id: ";

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    public List<MessageDto> retrieveAllMessages() {
        return makeMessageDtoListFromMessageList(messageRepository.findAll());
    }

    public MessageDto retrieveMessageById(Integer messageId) {
        Message message = messageRepository.findById(messageId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_MESSAGE_BY_ID + messageId));
        return MessageDto.messageDtoFromMessage(message);
    }

    public MessageDto createMessage(Integer fromId, Integer toId, MessageDto messageDto) {

        Message message = new Message();
        User sender = userRepository
                .findById(fromId)
                .orElseThrow(()->new ResourceNotFoundException("Not found sender id: " + fromId));

        User recipient = userRepository
                .findById(toId)
                .orElseThrow(()->new ResourceNotFoundException("Not found recipient id: " + toId));

        message.setSender(sender);
        message.setRecipient(recipient);
        message.setTopic(messageDto.getTopic());
        message.setMessageBody(messageDto.getMessageBody());
        message.setMessageTime(messageDto.getMessageTime().plusHours(3));
        messageRepository.save(message);
        return MessageDto.messageDtoFromMessage(message);
    }

    public ResponseEntity<String> deleteMessageById(Integer messageId) {
        Message message = messageRepository
                .findById(messageId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_MESSAGE_BY_ID + messageId));

        messageRepository.deleteById(messageId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public MessageDto updateMessageById(Integer messageId, MessageDto messageDto) {
        Message message = messageRepository
                .findById(messageId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_MESSAGE_BY_ID + messageId));

        if ((!messageDto.getMessageBody().isEmpty())
                && (!messageDto.getMessageBody().equals(message.getMessageBody()))) {
            message.setMessageBody(messageDto.getMessageBody());
        }

        if ((!messageDto.getTopic().isEmpty()) &&
                (!messageDto.getTopic().equals(message.getTopic()))) {
            message.setTopic(messageDto.getTopic());
        }

        message.setMessageTime(messageDto.getMessageTime().plusHours(3));

        messageRepository.save(message);
        return MessageDto.messageDtoFromMessage(message);
    }

    public List<MessageDto> retrieveMessagesByTwoUsers(Integer userOneId, Integer userTwoId) {
        return makeMessageDtoListFromMessageList(messageRepository.findByTwoUsers(userOneId, userTwoId));
    }

    private List<MessageDto> makeMessageDtoListFromMessageList(List<Message> messages) {
        List<MessageDto> messageDtoList = new ArrayList<>();
        for (Message message : messages) {
            messageDtoList.add(MessageDto.messageDtoFromMessage(message));
        }
        return messageDtoList;
    }
}
