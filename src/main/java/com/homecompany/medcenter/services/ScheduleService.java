package com.homecompany.medcenter.services;


import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.Schedule;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.ScheduleDto;
import com.homecompany.medcenter.repositories.DoctorRepository;
import com.homecompany.medcenter.repositories.ScheduleRepository;
import com.homecompany.medcenter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Service
public class ScheduleService {

    @Autowired
    private ScheduleRepository scheduleRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;

    public List<LocalTime> getDoctorTime(Integer doctorId, String date) {

        List<Schedule> scheduleList = scheduleRepository.findByDoctorId(doctorId);
        List<LocalTime> doctorFreeTime = new ArrayList<>();

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date1 = LocalDate.parse(date, dtf);

        for (Schedule schedule : scheduleList) {
            if ((schedule.getDate().toLocalDateTime().toLocalDate().equals(date1))
                    && (schedule.getPatient() == null)) {
                doctorFreeTime.add(schedule.getDate().toLocalDateTime().toLocalTime());
            }
        }
        return doctorFreeTime;
    }

    public ResponseEntity<String> updateDoctorSchedule(ScheduleDto scheduleDto) {
        Doctor doctor = doctorRepository.findById(scheduleDto.getDoctor()).orElseThrow(
                ()-> new ResourceNotFoundException("Not found doctor by id: " + scheduleDto.getDoctor()));

        if (scheduleDto.getPatient() == -1) {
            Schedule schedule = scheduleRepository.findByDoctorIdAndDate(scheduleDto.getDoctor(), Timestamp.valueOf(scheduleDto.getAppointment()));
            schedule.setPatient(null);
            scheduleRepository.save(schedule);
            return ResponseEntity.status(HttpStatus.OK).body(null);
        }

        User patient = userRepository.findById(scheduleDto.getPatient()).orElseThrow(
                ()-> new ResourceNotFoundException("Not found doctor by id: " + scheduleDto.getPatient()));

        /*EDIT STEP*/
        if ((scheduleDto.getOldAppointment() != null)
                && (!scheduleDto.getAppointment().equals(scheduleDto.getOldAppointment()))){
            Schedule oldSchedule = scheduleRepository.findByDoctorIdAndDate(doctor.getUser().getId(), Timestamp.valueOf(scheduleDto.getOldAppointment()));
            oldSchedule.setPatient(null);
            scheduleRepository.save(oldSchedule);
        }

        Schedule schedule = scheduleRepository.findByDoctorIdAndDate(doctor.getUser().getId(), Timestamp.valueOf(scheduleDto.getAppointment()));
        schedule.setPatient(patient);
        scheduleRepository.save(schedule);

        return ResponseEntity.status(HttpStatus.OK).body(null);
    }
}
