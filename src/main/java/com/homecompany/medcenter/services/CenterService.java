package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Center;
import com.homecompany.medcenter.models.Location;
import com.homecompany.medcenter.models.dto.CenterDto;
import com.homecompany.medcenter.repositories.CenterRepository;
import com.homecompany.medcenter.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CenterService {

    private static final String NOT_FOUND_CENTER_BY_ID = "Not found center by id: ";

    @Autowired
    private CenterRepository centerRepository;

    @Autowired
    private LocationRepository locationRepository;

    public ResponseEntity<List<Center>> retrieveAllCenters() {
        return ResponseEntity.ok().body(centerRepository.findAll());
    }

    public Center retrieveCenterById(Integer centerId) {
        return centerRepository
                .findById(centerId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_CENTER_BY_ID + centerId));
    }

    public Center createCenterWithLoc(Integer locId, Center center) {
        Location location = locationRepository
                .findById(locId)
                .orElseThrow(()->new ResourceNotFoundException("Not found location by id: " + locId));
        center.setLocation(location);

        return centerRepository.save(center);
    }

    public ResponseEntity<String> deleteCenterById(Integer centerId) {
        Center center = centerRepository
                .findById(centerId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_CENTER_BY_ID + centerId));

        centerRepository.deleteById(centerId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public Center updateCenterById(Integer centerId, CenterDto centerDto) {
        Center center = centerRepository
                .findById(centerId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_CENTER_BY_ID + centerId));

        Optional<Location> location = locationRepository
                .findById(centerDto.getLocationId());

        location.ifPresent(center::setLocation);

        return centerRepository.save(center);
    }
}
