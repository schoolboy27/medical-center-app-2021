package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.Service;
import com.homecompany.medcenter.models.Treatment;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.TreatmentDto;
import com.homecompany.medcenter.models.dto.UserDtoV2;
import com.homecompany.medcenter.repositories.DoctorRepository;
import com.homecompany.medcenter.repositories.ServiceRepository;
import com.homecompany.medcenter.repositories.TreatmentRepository;
import com.homecompany.medcenter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class TreatmentService {

    private static final String NOT_FOUND_TREATMENT_BY_ID = "Not found treatment by id: ";

    @Autowired
    private TreatmentRepository treatmentRepository;

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;

    public List<TreatmentDto> retrieveAllTreatments() {
        return makeTreatmentDtoListFromTreatmentList(treatmentRepository
                .findAll(Sort.by(Sort.Direction.DESC, "appointment")));
    }

    public TreatmentDto retrieveTreatmentById(Integer treatmentId) {
        Treatment treatment = treatmentRepository
                .findById(treatmentId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_TREATMENT_BY_ID + treatmentId));
        return TreatmentDto.treatmentDtoFromTreatment(treatment);
    }

    public TreatmentDto createTreatment(Integer serviceId, Integer doctorId, Integer patientId, LocalDateTime dateTime) {
        Treatment treatment = new Treatment();
        Doctor doctor = doctorRepository
                .findById(doctorId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found doctor by id: " + doctorId));
        User patient = userRepository
                .findById(patientId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found user by id: " + patientId));
        Service service = serviceRepository
                .findById(serviceId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found services by id: " + serviceId));

        treatment.setDoctor(doctor);
        treatment.setService(service);
        treatment.setPatient(patient);
        treatment.setAppointment(dateTime);

        treatmentRepository.save(treatment);
        return TreatmentDto.treatmentDtoFromTreatment(treatment);
    }

    public ResponseEntity<Object> deleteTreatmentById(Integer treatmentId) {
        Treatment treatment = treatmentRepository
                .findById(treatmentId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_TREATMENT_BY_ID + treatmentId));

        treatmentRepository.deleteById(treatmentId);
        return ResponseEntity.status(HttpStatus.OK).body(null);

    }

    public TreatmentDto updateTreatmentById(
            Integer treatmentId,
            Integer serviceId,
            Integer doctorId,
            Integer patientId,
            TreatmentDto treatmentDto) {
        Treatment treatment = treatmentRepository
                .findById(treatmentId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_TREATMENT_BY_ID + treatmentId));

        Optional<Service> service = serviceRepository.findById(serviceId);
        service.ifPresent(treatment::setService);

        Optional<Doctor> doctor = doctorRepository.findById(doctorId);
        doctor.ifPresent(treatment::setDoctor);

        Optional<User> patient = userRepository.findById(patientId);
        patient.ifPresent(treatment::setPatient);

        treatment.setAppointment(treatmentDto.getAppointment());
        treatmentRepository.save(treatment);
        return TreatmentDto.treatmentDtoFromTreatment(treatment);
    }

    public List<TreatmentDto> retrieveTreatmentsByPatientId(Integer patientId) {
        return makeTreatmentDtoListFromTreatmentList(treatmentRepository
                .findTreatmentsByPatientIdOrderByAppointmentDesc(patientId));
    }

    public List<TreatmentDto> retrieveTreatmentsByDoctorId(Integer doctorId) {
        return makeTreatmentDtoListFromTreatmentList(
                treatmentRepository.findTreatmentsByDoctorIdOrderByAppointmentDesc(doctorId));
    }

    public List<UserDtoV2> retrieveDoctorsByPatientId(Integer patientId) {
        List<Treatment> treatmentList =
                treatmentRepository.findTreatmentsByPatientIdOrderByAppointmentDesc(patientId);
        List<UserDtoV2> doctors = new ArrayList<>();
        for (Treatment treatment : treatmentList) {
            doctors.add(UserDtoV2.userDtoV2FromDoctor(treatment.getDoctor()));
        }
        return doctors;
    }

    public List<UserDtoV2> retrieveInterlocutorsForDoctor(Integer doctorId) {
        List<Treatment> treatmentList =
                treatmentRepository.findTreatmentsByDoctorIdOrderByAppointmentDesc(doctorId);

        List<UserDtoV2> interlocutors = new ArrayList<>();

        List<Doctor> doctorList = doctorRepository.findAll();

        List<User> admins = userRepository.findAdmins();

        for (Doctor doctor : doctorList) {
            interlocutors.add(UserDtoV2.userDtoV2FromDoctor(doctor));
        }
        for (Treatment treatment : treatmentList) {
            interlocutors.add(UserDtoV2.userDtoV2FromUser(treatment.getPatient()));
        }
        for (User admin : admins) {
            interlocutors.add(UserDtoV2.userDtoV2FromAdmin(admin));
        }
        return interlocutors;
    }

    private List<TreatmentDto> makeTreatmentDtoListFromTreatmentList(List<Treatment> treatmentList) {
        List<TreatmentDto> treatmentDtoList = new ArrayList<>();
        for (Treatment treatment : treatmentList) {
            treatmentDtoList.add(TreatmentDto.treatmentDtoFromTreatment(treatment));
        }
        return treatmentDtoList;
    }
}
