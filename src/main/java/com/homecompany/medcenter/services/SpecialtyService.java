package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Specialty;
import com.homecompany.medcenter.models.dto.SpecialtyDto;
import com.homecompany.medcenter.repositories.SpecialtyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

@Service
public class SpecialtyService {

    private static final String NOT_FOUND_SPECIALTY_BY_ID = "Not found specialty by id: ";

    @Autowired
    private SpecialtyRepository specialtyRepository;

    public List<Specialty> retrieveAllSpecialties() {
        return specialtyRepository.findAll();
    }

    public Specialty retrieveSpecialtyById(Integer specialtyId) {
        return specialtyRepository
                .findById(specialtyId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_SPECIALTY_BY_ID + specialtyId));
    }

    public Specialty createSpecialty(Specialty specialty) {
        return specialtyRepository.save(specialty);
    }

    public Specialty updateSpecialty(SpecialtyDto specialtyDto) {
        Specialty specialty = specialtyRepository
                .findById(specialtyDto.getId())
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_SPECIALTY_BY_ID + specialtyDto.getId()));
        if (!specialtyDto.getName().isEmpty()) {
            specialty.setName(specialtyDto.getName());
        }
        return specialtyRepository.save(specialty);
    }

    public ResponseEntity<Object> deleteSpecialtyById(Integer specialtyId) {
        Specialty specialty = specialtyRepository
                .findById(specialtyId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_SPECIALTY_BY_ID + specialtyId));

        specialtyRepository.deleteById(specialtyId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }
}
