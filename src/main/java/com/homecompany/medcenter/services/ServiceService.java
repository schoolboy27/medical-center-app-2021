package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Service;
import com.homecompany.medcenter.models.Specialty;
import com.homecompany.medcenter.models.dto.ServiceDto;
import com.homecompany.medcenter.repositories.ServiceRepository;
import com.homecompany.medcenter.repositories.SpecialtyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.List;
import java.util.Optional;

@org.springframework.stereotype.Service
public class ServiceService {

    private static final String NOT_FOUND_SERVICE_BY_ID = "Not found service by id: ";

    @Autowired
    private ServiceRepository serviceRepository;

    @Autowired
    private SpecialtyRepository specialtyRepository;

    public List<Service> retrieveAllServices() {
        return serviceRepository.findAll();
    }

    public Service retrieveServiceById(Integer serviceId) {
        return serviceRepository
                .findById(serviceId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_SERVICE_BY_ID + serviceId));
    }

    public Service createServiceWithSpecialty(Integer specialtyId, Service service) {
        Specialty specialty = specialtyRepository
                .findById(specialtyId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found specialty by id: " + specialtyId));
        service.setSpecialty(specialty);
        return serviceRepository.save(service);
    }

    public ResponseEntity<String> deleteServiceById(Integer serviceId) {
        Service service = serviceRepository
                .findById(serviceId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_SERVICE_BY_ID + serviceId));

        serviceRepository.deleteById(serviceId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public Service updateServiceById(Integer serviceId, Integer specialtyId, ServiceDto serviceDto) {
        Service service = serviceRepository
                .findById(serviceId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_SERVICE_BY_ID + serviceId));

        Optional<Specialty> specialty = specialtyRepository
                .findById(specialtyId);

        specialty.ifPresent(service::setSpecialty);

        if (!serviceDto.getName().isEmpty()) {
            service.setName(serviceDto.getName());
        }

        if ((Double.compare(service.getPrice(),serviceDto.getPrice()) != 0) &&
                (serviceDto.getPrice() != 0)) {
            service.setPrice(serviceDto.getPrice());
        }

        return serviceRepository.save(service);
    }

    public List<Service> retrieveServicesBySpecialtyId(Integer specialtyId) {
        return serviceRepository.findBySpecialtyId(specialtyId);
    }
}
