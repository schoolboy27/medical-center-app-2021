package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.Review;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.ReviewDto;
import com.homecompany.medcenter.models.dto.ReviewDtoForUser;
import com.homecompany.medcenter.repositories.DoctorRepository;
import com.homecompany.medcenter.repositories.ReviewRepository;
import com.homecompany.medcenter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ReviewService {

    private static final String NO_FOUND_REVIEW_BY_ID = "Not found review by id: ";

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;

    public List<ReviewDto> retrieveAllReviews() {
        List<Review> reviewList = reviewRepository.findAll();
        List<ReviewDto> reviewDtoList = new ArrayList<>();
        for (Review review: reviewList) {
            reviewDtoList.add(ReviewDto.reviewDtoFromReview(review));
        }
        return reviewDtoList;
    }

    public ReviewDto retrieveReviewById(Integer reviewId) {
        Review review = reviewRepository
                .findById(reviewId)
                .orElseThrow(()-> new ResourceNotFoundException(NO_FOUND_REVIEW_BY_ID + reviewId));
        return ReviewDto.reviewDtoFromReview(review);
    }

    public ReviewDto createReview(Integer doctorId, Integer patientId, Review review) {
        Doctor doctor = doctorRepository
                .findById(doctorId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found doctor by id: " + doctorId));
        User patient = userRepository
                .findById(patientId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found user by id: " + patientId));

        review.setPatient(patient);
        review.setDoctor(doctor);
        reviewRepository.save(review);
        return ReviewDto.reviewDtoFromReview(review);
    }

    public ResponseEntity<String> deleteReviewById(Integer reviewId) {
        Review review = reviewRepository
                .findById(reviewId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found review by id: " + reviewId));

        reviewRepository.deleteById(reviewId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public ReviewDto updateReviewById(Integer reviewId, Integer doctorId, Integer patientId, ReviewDto reviewDto) {
        Review review = reviewRepository
                .findById(reviewId)
                .orElseThrow(()-> new ResourceNotFoundException(NO_FOUND_REVIEW_BY_ID + reviewId));

        if (review.getDoctor().getId() != doctorId) {
            Optional<Doctor> doctor = doctorRepository.findById(doctorId);
            doctor.ifPresent(review::setDoctor);
        }

        if (!review.getPatient().getId().equals(patientId)) {
            Optional<User> patient = userRepository.findById(patientId);
            patient.ifPresent(review::setPatient);
        }

        if ((!reviewDto.getReviewBody().isEmpty())
                && (!reviewDto.getReviewBody().equals(review.getReviewBody()))) {
            review.setReviewBody(reviewDto.getReviewBody());
        }

        reviewRepository.save(review);
        return ReviewDto.reviewDtoFromReview(review);
    }

    public List<ReviewDtoForUser> retrieveAllReviewsToUser() {
        List<Review> reviewList = reviewRepository.findAll();
        List<ReviewDtoForUser> reviewDtoList = new ArrayList<>();

        for (Review review : reviewList) {
            reviewDtoList.add(ReviewDtoForUser.reviewDtoForUserFromReview(review));
        }
        return reviewDtoList;

    }
}
