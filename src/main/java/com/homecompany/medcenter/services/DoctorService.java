package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Center;
import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.Specialty;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.DoctorDto;
import com.homecompany.medcenter.models.dto.DoctorDtoV3;
import com.homecompany.medcenter.repositories.CenterRepository;
import com.homecompany.medcenter.repositories.DoctorRepository;
import com.homecompany.medcenter.repositories.SpecialtyRepository;
import com.homecompany.medcenter.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class DoctorService {

    private static final String NOT_FOUND_DOCTOR_BY_ID = "Not found doctor by id: ";

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SpecialtyRepository specialtyRepository;

    @Autowired
    private CenterRepository centerRepository;

    public List<DoctorDtoV3> retrieveAllDoctors() {
        List<Doctor> doctorList = doctorRepository.findAll();
        List<DoctorDtoV3> doctorDtoList = new ArrayList<>();
        for(Doctor doctor : doctorList) {
            doctorDtoList.add(DoctorDtoV3.doctorDtoFromDoctor(doctor));
        }
        return doctorDtoList;
    }

    public DoctorDtoV3 retrieveDoctorById(Integer doctorId) {
        return DoctorDtoV3.doctorDtoFromDoctor( doctorRepository
                .findById(doctorId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_DOCTOR_BY_ID + doctorId)));
    }

    public List<DoctorDto> retrieveDoctorsBySpecialtyId(Integer specialtyId) {
        return makeDoctorDtoListFromDoctorList(doctorRepository.findBySpecialtyId(specialtyId));
    }


    public DoctorDtoV3 createDoctor(Integer userId, Integer specialtyId, Integer centerId, DoctorDtoV3 doctorDto) {

        Doctor doctor = new Doctor();

        User user = userRepository
                .findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found user by id: " + userId));
        Specialty specialty = specialtyRepository
                .findById(specialtyId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found spec by id: " + specialtyId));
        Center center = centerRepository
                .findById(centerId)
                .orElseThrow(()-> new ResourceNotFoundException("Not found center by id: " + centerId));

        doctor.setUser(user);
        doctor.setCenter(center);
        doctor.setSpecialty(specialty);
        doctor.setSalary(doctorDto.getSalary());
        doctorRepository.save(doctor);

        return DoctorDtoV3.doctorDtoFromDoctor(doctor);

    }

    public ResponseEntity<String> deleteDoctorById(Integer doctorId) {
        Doctor doctor = doctorRepository
                .findById(doctorId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_DOCTOR_BY_ID + doctorId));

        doctorRepository.deleteById(doctorId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public DoctorDtoV3 updateDoctorById(Integer doctorId, Integer centerId, Integer specialtyId, DoctorDtoV3 doctorDto) {

        Doctor doctor = doctorRepository
                .findById(doctorId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_DOCTOR_BY_ID + doctorId));

        if (doctorDto.getSalary() != 0) {
            doctor.setSalary(doctorDto.getSalary());
        }

        if (!doctor.getSpecialty().getId().equals(specialtyId)) {
            Optional<Specialty> specialty = specialtyRepository.findById(specialtyId);
            specialty.ifPresent(doctor::setSpecialty);
        }

        if (!doctor.getCenter().getId().equals(centerId)) {
            Optional<Center> center = centerRepository.findById(centerId);
            center.ifPresent(doctor::setCenter);
        }

        doctorRepository.save(doctor);
        return DoctorDtoV3.doctorDtoFromDoctor(doctor);
    }

    public List<DoctorDto> retrieveAllDoctorsToUser() {
        return makeDoctorDtoListFromDoctorList(doctorRepository.findAll());
    }

    private List<DoctorDto> makeDoctorDtoListFromDoctorList(List<Doctor> doctors) {
        List<DoctorDto> doctorDtoList = new ArrayList<>();
        for (Doctor doc: doctors) {
            doctorDtoList.add(DoctorDto.doctorDtoFromDoctor(doc));
        }

        return doctorDtoList;
    }
}
