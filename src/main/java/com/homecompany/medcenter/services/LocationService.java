package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.Location;
import com.homecompany.medcenter.models.dto.LocationDto;
import com.homecompany.medcenter.repositories.LocationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LocationService {

    private static final String NOT_FOUND_LOCATION_BY_ID = "Not found location by id: ";

    @Autowired
    private LocationRepository locationRepository;

    public List<Location> retrieveAllLocations() {
        return locationRepository.findAll();
    }

    public Location retrieveLocationById(Integer locationId) {
        return locationRepository
                .findById(locationId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_LOCATION_BY_ID + locationId));
    }

    public Location createLocation(Location location) {
        return locationRepository.save(location);
    }


    public ResponseEntity<String> deleteLocationById(Integer locationId) {
        Location location = locationRepository
                .findById(locationId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_LOCATION_BY_ID + locationId));

        locationRepository.deleteById(locationId);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public Location updateLocationById(Integer locationId, LocationDto locationDto) {
        Location locationToBeUpdated = locationRepository
                .findById(locationId)
                .orElseThrow(()->new ResourceNotFoundException(NOT_FOUND_LOCATION_BY_ID + locationId));

        if (!locationDto.getStreet().isEmpty()) {
            locationToBeUpdated.setStreet(locationDto.getStreet());
        }

        if (!locationDto.getBuilding().isEmpty()) {
            locationToBeUpdated.setBuilding(locationDto.getBuilding());
        }

        if (locationDto.getApartment() > 0) {
            locationToBeUpdated.setApartment(locationDto.getApartment());
        }

        return locationRepository.save(locationToBeUpdated);
    }
}
