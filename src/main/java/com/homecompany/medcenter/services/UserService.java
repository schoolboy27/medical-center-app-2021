package com.homecompany.medcenter.services;

import com.homecompany.medcenter.models.AuthBody;
import com.homecompany.medcenter.models.Center;
import com.homecompany.medcenter.models.Doctor;
import com.homecompany.medcenter.models.User;
import com.homecompany.medcenter.models.dto.UserDto;
import com.homecompany.medcenter.models.dto.UserDtoV2;
import com.homecompany.medcenter.repositories.CenterRepository;
import com.homecompany.medcenter.repositories.DoctorRepository;
import com.homecompany.medcenter.repositories.UserRepository;
import com.homecompany.medcenter.security.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.*;


@Service
@Transactional
public class UserService {

    private static final String NOT_FOUND_USER_BY_ID = "Not found user by id: ";

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DoctorRepository doctorRepository;

    @Autowired
    private CenterRepository centerRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    public List<UserDto> retrieveAllUsers() {
        List<User> userList = userRepository.findAll();
        List<UserDto> userDtoList = new ArrayList<>();
        for(User user : userList) {
            userDtoList.add(UserDto.userDtoFromUser(user));
        }
        return userDtoList;
    }

    public UserDto retrieveUserById(Integer userId) {
        User user = userRepository
                .findById(userId)
                .orElseThrow(()-> new ResourceNotFoundException(NOT_FOUND_USER_BY_ID + userId));
        return UserDto.userDtoFromUser(user);
    }

    public UserDto retrieveUserByUsername(String username) {
        User user = userRepository.findByLogin(username).orElseThrow(
                () -> new ResourceNotFoundException("Not found user by username " + username)
        );
        return UserDto.userDtoFromUser(user);
    }

    public List<UserDtoV2> retrieveAllInterlocutorsForAdmin() {

        List<User> users = userRepository.findAllUsersNotDoctors();
        List<Doctor> doctors = doctorRepository.findAll();

        List<UserDtoV2> interlocutors = new ArrayList<>();

        for (Doctor doctor : doctors) {
            interlocutors.add(UserDtoV2.userDtoV2FromDoctor(doctor));
        }
        for (User user : users) {
            interlocutors.add(UserDtoV2.userDtoV2FromUser(user));
        }
        return interlocutors;
    }

    public List<UserDtoV2> retrieveUsersNotDoctors() {
        List<User> users = userRepository.findAllUsersNotDoctors();

        List<UserDtoV2> userDtoList = new ArrayList<>();
        for (User user : users) {
            userDtoList.add(UserDtoV2.userDtoV2FromUser(user));
        }
        return userDtoList;
    }

    public ResponseEntity<Object> createUser(User user) {

        Optional<User> newUser = userRepository.findByLogin(user.getLogin());
        if (newUser.isPresent()) {
            return ResponseEntity.badRequest().body(null);
        }

        if ((!user.getUserRole().equals("ROLE_USER"))
                && (!user.getUserRole().equals("ROLE_ADMIN"))
                && (!user.getUserRole().equals("ROLE_DOCTOR"))) {
            return ResponseEntity.badRequest().body(null);
        }

        Center center = centerRepository.findById(user.getCenter().getId()).orElseThrow(
                () -> new ResourceNotFoundException("Not found center by id: " + user.getCenter().getId())
        );
        user.setCenter(center);
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return ResponseEntity.ok((new HashMap<>().put("User is created. Login: ", user.getLogin())));
    }

    public ResponseEntity<Object> deleteUserById(Integer userId) {
        User user = userRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(NOT_FOUND_USER_BY_ID + userId));
        userRepository.delete(user);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    public UserDto updateUserById(Integer userId, UserDto userDto) {
        User user = userRepository.
                findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(NOT_FOUND_USER_BY_ID + userId));

        if ((userDto.getLastName() != null) && (!userDto.getLastName().isEmpty())) {
            user.setLastName(userDto.getLastName());
        }
        if ((userDto.getFirstName() != null) && (!userDto.getFirstName().isEmpty())) {
            user.setFirstName(userDto.getFirstName());
        }
        if ((userDto.getMiddleName() != null) && (!userDto.getMiddleName().isEmpty())) {
            user.setMiddleName(userDto.getMiddleName());
        }

        if ((userDto.getBirthDate() != null) && (userDto.getBirthDate().toString().length() == 10)) {
            user.setBirthDate(userDto.getBirthDate());
        }

        if ((userDto.getUserRole() != null)
                && (!userDto.getUserRole().isEmpty())
                && ((userDto.getUserRole().equals("ROLE_ADMIN"))
                || (userDto.getUserRole().equals("ROLE_DOCTOR"))
                || (userDto.getUserRole().equals("ROLE_USER")))) {
            user.setUserRole(userDto.getUserRole());
        }

        user.setCenter(userDto.getCenter());
        userRepository.save(user);
        return UserDto.userDtoFromUser(user);
    }

    public ResponseEntity<Object> doLogin(AuthBody authBody, HttpServletRequest request) {
        try {
            Authentication authentication = authenticationManager
                    .authenticate(
                            new UsernamePasswordAuthenticationToken(authBody.getUsername(), authBody.getPassword()));

            User user = userRepository
                    .findByLogin(authBody.getUsername())
                    .orElseThrow(()-> new UsernameNotFoundException("Not username"));
            String token = jwtTokenProvider.createToken(authBody.getUsername(), user.getUserRole());
            Map<Object, Object> response = new HashMap<>();
            response.put("username", authBody.getUsername());
            response.put("token", token);
            return ResponseEntity.ok(response);

        } catch (AuthenticationException e) {
            return new ResponseEntity<>("Invalid username or password", HttpStatus.BAD_REQUEST);
        }
    }

}
